import React from 'react';
import { get_phones_from_API, get_phone_data } from '../helpers/ApiHelper';


export default class Phones extends React.Component {

    constructor(props) {
        super(props);
        this.store = this.props.store;
        this.phone_id = this.props.params.phone_id;

    }

    state = {
        o_phone_element: {}
    }

    componentDidMount() {

        const b_phones = this.store.getState().a_phones.length > 0;
        const o_state_phone = this.store.getState().o_phone;
        
        if (!b_phones) {
            get_phones_from_API(this.store)
        }
       
        if (b_phones && (Object.keys(o_state_phone).length === 0 || parseInt(o_state_phone.id) !== parseInt(this.phone_id))) {
            
            get_phone_data(this.store, this.phone_id);

        }  
        
    }
    
    
    render() {
        const b_phones = this.store.getState().a_phones.length === 0;
        const o_state_phone = this.store.getState().o_phone;

        let is_loading = b_phones || (Object.keys(o_state_phone).length === 0);
    
        
        
        return (
            <div className="row">
                {is_loading && (
                    <img className="mx-auto" src={require("../images/loader.gif")} alt="Loading..." />
                )}

                {!is_loading && (
                    <div>
                        <h1 className="text-center">{this.store.getState().o_phone.name}</h1>
                        <div className="col-12 row justify-content-center">
                            <div className="col-12 col-md-4">
                                
                                <img className="col-12 text-center" src={require(`../images/${this.store.getState().o_phone.imageFileName}`)} alt={this.store.getState().o_phone.imageFileName} />
                                
                            </div>

                            <div className="col-12 col-md-4 my-auto">
                                {this.store.getState().o_phone.description}
                                
                            </div>
                        </div>
                    

                        <h3 className="col-12 text-center">
                                Specifications
                        </h3>

                        <table className="table table-striped mt-2 col-12 col-md-10 mx-auto">
                            <tbody>
                                <tr className="text-center">
                                    <th>Manufacturer</th>
                                    <td>{this.store.getState().o_phone.manufacturer}</td>
                                </tr>
                                <tr className="text-center">
                                    <th>Color</th>
                                    <td className="text-capitalize">{this.store.getState().o_phone.color}</td>
                                </tr>
                                <tr className="text-center">
                                    <th>Price</th>
                                    <td>${this.store.getState().o_phone.price}</td>
                                </tr>
                                <tr className="text-center">
                                    <th>Screen</th>
                                    <td>{this.store.getState().o_phone.screen}</td>
                                </tr>
                                <tr className="text-center">
                                    <th>Processor</th>
                                    <td>{this.store.getState().o_phone.processor}</td>
                                </tr>
                                <tr className="text-center">
                                    <th>Ram</th>
                                    <td>{this.store.getState().o_phone.ram} GB</td>
                                </tr>
                            </tbody>
                        </table>
                        
                    </div>
                    
                )}
            </div>
            
        )
    }

    
}
