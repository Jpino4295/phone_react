import React from 'react';
import { BrowserRouter as Router, Switch, Route, Link } from 'react-router-dom';

export default class PhoneCard extends React.Component {

    constructor(props) {
      super(props);
      this.props = props;
      this.store = props.store;
      
    }

    
    render () {
      return (
        <div className="col-12 col-md-4">
          <Link to={`/phone/${this.props.o_phone.id}`}>
            
            
            <div className="card mb-2 mr-2 display-inline">
              <div className="card-body row">
              <div className="col-4">
                  <img className="col-12" src={require(`../images/${this.props.o_phone.imageFileName}`)} alt={this.props.o_phone.imageFileName} />
                </div>

                <div className="col-8">
                  <span className="display-inline-block">{this.props.o_phone.name}</span>
                  <br />
                  <span className="text-muted display-inline-block small">({ this.props.o_phone.manufacturer })</span>
                </div>
              </div>
            </div>
            
            
          </Link>
        </div>  
      );
    }
}
