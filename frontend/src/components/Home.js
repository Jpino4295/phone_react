import React from 'react';
import PhoneCard from './PhoneCard';
import { get_phones_from_API } from '../helpers/ApiHelper';

export default class Home extends React.Component {

    constructor(props) {
        super(props);
        this.store = this.props.store;
    }

    componentDidMount() {
        if (this.store.getState().is_loading) {
            get_phones_from_API(this.store)
        }
        
    }

    render() {
        let is_loading = this.store.getState().is_loading;

        return (
            <div className="row">
            {is_loading && (
                <img className="mx-auto" src={require("../images/loader.gif")} alt="Loading..." />
            )}
            {!is_loading && this.store.getState().a_phones.map((o_phone) => (
                
                <PhoneCard o_phone={o_phone} key={o_phone.id} />
            ))}
           
            </div>    
            
            
        )
    }

    
}
