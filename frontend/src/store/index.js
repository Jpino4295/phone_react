const initial_state = {
    a_phones: [],
    is_loading: true,
    o_phone: {}
};


export default (state = initial_state, action) => {
    switch(action.type) {
        case 'SET_PHONES':
            return Object.assign({}, state, {
                a_phones: action.a_phones
            })
        case 'SET_PHONE_DATA':
            return Object.assign({}, state, {
                o_phone: action.o_phone
            })
        case 'SET_LOADED':
            return Object.assign({}, state, {
                is_loading: false
            })
        case 'SET_LOADING':
            return Object.assign({}, state, {
                is_loading: true
            })
        case 'CLEAR_DATA':
            return Object.assign({}, state, initial_state)
        default:
            return state;
    }
}