export const set_phones = (a_phones) => {
    return {
        type: "SET_PHONES",
        a_phones
    }
}

export const set_loaded = () => {
    return {
        type: "SET_LOADED"
    }
}

export const set_loading = () => {
    return {
        type: "SET_LOADING"
    }
}

export const set_phone_data = (o_phone) => {
    return  {
        type: "SET_PHONE_DATA",
        o_phone
    }
}

export const clear_data = (o_phone) => {
    return  {
        type: "CLEAR_DATA",
        o_phone
    }
}