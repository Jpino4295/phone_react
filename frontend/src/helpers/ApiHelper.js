import Axios from 'axios';
import Config from '../config/config';
import { set_phones, set_loaded, set_phone_data } from '../store/actions'

export const get_phones_from_API = (store) => {
    const s_url =`${Config.s_public_protocol}://${Config.s_base_url}:${Config.n_port}/phones`
    
    Axios.get(s_url).then((o_phones_response) => {
        let a_phones_element = [];

        for (let o_phone of o_phones_response.data) {
            a_phones_element.push(o_phone)
        }

        store.dispatch(set_phones(a_phones_element))
        store.dispatch(set_loaded())

        
    })
    
}

export const get_phone_data = (store, phone_id) => {
    const s_phone_id = phone_id;

    let a_phones = store.getState().a_phones;

    const o_phone = a_phones.filter((o_phone) => {
        return parseInt(o_phone.id) === parseInt(s_phone_id)
    }).shift();
    
    store.dispatch(set_phone_data(o_phone))
    
    store.dispatch(set_loaded())
}