import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import { BrowserRouter as Router, Route, Link, useRouteMatch, useParams } from "react-router-dom";
import { createStore } from 'redux';
import Actions from './store';


let store = createStore(Actions);

function render() {
  ReactDOM.render(
    <React.StrictMode>
      <Router>
        <App store={store}/>
        
      </Router>
    </React.StrictMode>,
    document.getElementById('root')
  );
}


store.subscribe(render)

render()
