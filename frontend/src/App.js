import React from 'react';
import Home from './components/Home';
import PhoneDetail from './components/PhoneDetail';
import Header from './components/Header';
import { BrowserRouter as Router, Route, Link, useRouteMatch, useParams } from "react-router-dom";

import 'bootstrap/dist/css/bootstrap.css';
import './App.css';

class App extends React.Component {
  constructor(props) {
    super(props);
    this.store = this.props.store;
  }
  
  render() {
    return (
      
        <div className="App">
            <Header />
          <div className="container mb-3">
            {/* <Home store={this.store}/> */}
            <Route exact path="/" component={() => <Home store={this.store} />} />
          <Route path="/phone/:phone_id" component={() => <PhoneDetail store={this.store} params={useParams()}/>} />
          </div>
            
        </div>

      
    )
  };

  
}

export default App;
