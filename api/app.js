'use strict';
process.env.NODE_PATH = __dirname;
require('module').Module._initPaths();
const o_config = require("config/config");
const express = require('express');
const o_bparser = require("body-parser");
const RouterClass = require('helpers/RouterHelper');

const o_app = express();

// Head
o_app.disable('x-powered-by');
o_app.enable('trust proxy');
o_app.set('trust proxy', ['loopback', 'linklocal', 'uniquelocal', '0.0.0.0']);

// Body Parser
o_app.use(o_bparser.json({ limit: '10mb' }));
o_app.use(o_bparser.urlencoded({ extended: true }));

// crossDomain
o_app.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Connection", "keep-alive"); // Para que no nos tire jquery
    res.header("Access-Control-Allow-Methods", "HEAD,PUT,POST,GET,DELETE,OPTIONS");
    res.header("Access-Control-Allow-Headers", "Origin, Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With, Accept");
  
    next();
  });

// Routes
const o_router = new RouterClass(express);

o_app.use('/', o_router.o_router, (err, res) => {
    if (err) {
        return res.status(404).send("Ups, 404");
    }
});


// Start app
var server = o_app.listen(o_config.n_port, () => {
    const s_host = server.address().address;
    const n_port = server.address().port;

    return console.log(`Listening at ip ${s_host} - ${o_config.s_public_protocol}${o_config.s_domain}:${n_port}`);
});
