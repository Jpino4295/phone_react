'use strict';
const Phones = require("data/phones.json");

class PhoneController {
    
    constructor(req, res) {
        this.req = req;
        this.res = res;

    }

    jsonAction() {
      setTimeout(() => {
        this.res.json(Phones);
        this.res.end()
      }, 2000);
      
    }
    
}

module.exports = PhoneController;