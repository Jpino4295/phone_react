const { expect } = require("chai");
const axios = require("axios");

describe("PhoneController", () => {
    it("Must return phones list", function(done) {
        this.timeout(10000);
        
        const s_url = `http://api:4000/phones`

        axios.get(s_url).then((o_axios_response) => {
            const a_data = o_axios_response.data;

            expect(a_data).to.be.an("array");
            expect(a_data).to.have.length.greaterThan(0);
            
            done();
        })
        
    })

})