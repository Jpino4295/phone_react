'use strict';

class RouterHelper {

    constructor(express) {
        
        this.o_router = express.Router();
        
        this.o_router.use( (req, res, next) => {
            this.req = req;
            this.res = res;
            
            this.set_local_routes();

            next();
        } )

    }


    launch( {s_controller, s_action, req, res} ) {
        const ControllerClass = require(`controllers/${s_controller}Controller`);
        let o_controller = new ControllerClass(req, res);

        o_controller[`${s_action}Action`]();
        o_controller = null;
    }


    set_local_routes() {
        this.o_router
            .get("/phones", (req, res) => {
                this.launch({ s_controller: "Phone", s_action: "json", req, res })
            })
    }
    
}



module.exports = RouterHelper;