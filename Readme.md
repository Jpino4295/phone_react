# React Phone
Simple webpage created with react and a backend api created with nodejs and express.

## Create containers
To start the service you must create the containers with

`docker-compose up -d frontend`

and then visit `localhost:8080` in your browser to see the page.


## Tests
To run the tests, you must run the next command

`docker-compose run tests`

This will start (or create) the api container and the test container.